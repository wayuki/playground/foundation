# Playground - Foundation
Shared components served by [Orchestrator](https://gitlab.com/wayuki/playground/orchestrator)

## Setup

### How do I spin up Foundation locally?
1. Clone this repository.
    ```PowerShell
    # Windows Powershell
    git clone git@gitlab.com:wayuki/playground/foundation.git
    cd .\foundation
    ```
    ```Shell
    # Unix Shell
    git clone git@gitlab.com:wayuki/playground/foundation.git
    cd ./foundation
    ```
2. Create a file `.env` with the content copied from `.env.example`, and fill out the values.
   1. `PUBLIC_URL` is a full URL which a browser can access the foundation, e.g. `http://localhost:3000`.
   2. `PORT` is the port which the foundation will be served in internally.

    <details>
      <summary markdown="span">Note</summary>
      Keep in mind that the port in `PORT` does not have to match the port in `PUBLIC_URL` if you are using Docker.

      For example, if you want the foundation to be served on port `3000` inside the Docker container, but want to map the port to `3333` on the host machine, the `PORT` should be `3000` while `PUBLIC_URL` should be `http://localhost:3333`.
    </details>
3. [Only when using Docker] Create a file `docker-compose.override.yml` with the content copied from `docker-compose.override.yml.example`, and customize it to your liking.
4. [Only when using Yarn on host] Run `yarn`.
5. Spin up Foundation.
    ```Shell
    # Use Docker
    docker-compose up local
    # Use NodeJS
    node start
    ```
