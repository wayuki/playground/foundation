/* eslint-disable */

import React, { PureComponent } from 'react';
import { Router } from 'react-router-dom';
import { getServiceProps } from 'service-props';
import * as uuid from 'uuid';
import logger from 'logger';
import TopNavigation from './components/TopNavigation';

interface INavigationState {
  error: Error | null
}

class Navigation extends PureComponent<{}, INavigationState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      error: null,
    };
  }

  public static getDerivedStateFromError(error: Error) {
    return {
      error,
    };
  }

  public componentDidCatch() {}

  public render() {
    const { singleSpa, router: { history } } = getServiceProps();

    const { error } = this.state;
    if (error) {
      const id = uuid.v4();
      logger.error(error, { id });

      if (window.location.pathname !== '/500') {
        singleSpa.navigateToUrl(`/500?error-id=${id}`);
      }
    }

    return (
      <Router history={history}>
        <TopNavigation />
      </Router>
    );
  }
}

export default Navigation;
