import { Menu } from 'antd';
import Container from 'common/Container';
import Link, { routes } from 'common/Link';
import React, { useMemo } from 'react';
import { matchPath, useLocation } from 'react-router-dom';
import style from './index.module.scss';

const TopNavigation: React.FC = () => {
  const location = useLocation();

  const selected = useMemo(
    () => (
      Object.entries(routes)
        .filter(([key, path]) => matchPath(location.pathname, { path, exact: key === 'home' }))
        .map(([key]) => key)
    ),
    [location],
  );

  return (
    <div className={style['top-nav-container']}>
      <Container>
        <div className={style['top-nav']}>
          <Menu
            selectedKeys={selected}
            mode="horizontal"
            className={style.menu}
          >
            <Menu.Item key="home">
              <Link to="home">Home</Link>
            </Menu.Item>
            <Menu.Item key="about">
              <Link to="about">About</Link>
            </Menu.Item>
          </Menu>
        </div>
      </Container>
    </div>
  );
};

export default TopNavigation;
