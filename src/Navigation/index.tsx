import React from 'react';
import ReactDOM from 'react-dom';
import singleSpaReact from 'single-spa-react';
import { IServiceProps, setServiceProps } from 'service-props';
import Navigation from './Navigation';

const domElementGetter = () => document.querySelector('#navigation')!;

const reactLifeCycles = singleSpaReact({
  React,
  ReactDOM,
  rootComponent: Navigation,
  domElementGetter,
});

const bootstrap = (props: IServiceProps) => {
  setServiceProps(props);

  return reactLifeCycles.bootstrap(props);
};

const { mount, unmount } = reactLifeCycles;

export {
  bootstrap,
  mount,
  unmount,
};
