import { Result, Typography } from 'antd';
import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { qs } from 'utils';
import PropTypes from 'prop-types';
import { getServiceProps } from 'service-props';

interface IGeneralErrorPageProps extends RouteComponentProps {}

const GeneralErrorPage: React.FC<IGeneralErrorPageProps> = ({ location: { search } }) => {
  const { pageManagement } = getServiceProps();
  const { 'error-id': errorId } = qs.parse<{ 'error-id'?: string }>(search);

  pageManagement.setPageTitle('Error');

  return (
    <Result
      status="500"
      title={(
        <Typography.Title>Aww snap.</Typography.Title>
      )}
      subTitle={(
        <>
          <Typography.Paragraph>
            Sorry, we encountered an error.
          </Typography.Paragraph>
          <Typography.Paragraph>
            Please contact the system administrator with the following reference:
          </Typography.Paragraph>
          <Typography.Paragraph code>
            {errorId}
          </Typography.Paragraph>
        </>
      )}
    />
  );
};

GeneralErrorPage.propTypes = {
  location: PropTypes.shape({
    search: PropTypes.string,
  }).isRequired,
} as React.WeakValidationMap<IGeneralErrorPageProps>;

export default GeneralErrorPage;
