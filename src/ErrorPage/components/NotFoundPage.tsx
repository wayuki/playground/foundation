import { Result, Typography } from 'antd';
import React from 'react';
import PropTypes from 'prop-types';
import { RouteComponentProps } from 'react-router-dom';
import { getServiceProps } from 'service-props';

interface INotFoundPageProps extends RouteComponentProps {}

const NotFoundPage : React.FC<INotFoundPageProps> = () => {
  const { pageManagement } = getServiceProps();
  pageManagement.setPageTitle('Not Found');

  return (
    <Result
      status="404"
      title={(
        <Typography.Title>Page Not Found</Typography.Title>
        )}
      subTitle={(
        <>
          <Typography.Paragraph>
            This is unfortunate. The page you were looking for doesn&#39;t exist.
          </Typography.Paragraph>
          <Typography.Paragraph>
            If you think this page should exist, please contact the system administrator.
          </Typography.Paragraph>
        </>
        )}
    />
  );
};

NotFoundPage.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
} as React.WeakValidationMap<INotFoundPageProps>;

export default NotFoundPage;
