import { Result, Typography } from 'antd';
import React from 'react';
import { getServiceProps } from 'service-props';

const AccessDeniedPage = () => {
  const { pageManagement } = getServiceProps();
  pageManagement.setPageTitle('Access Denied');

  return (
    <Result
      status="403"
      title={(
        <Typography.Title>Access Denied</Typography.Title>
      )}
      subTitle={(
        <>
          <Typography.Paragraph>
            It looks like you don&#39;t have permission to view this pages.
          </Typography.Paragraph>
          <Typography.Paragraph>
            If you think you should have permission to view this page, please contact
            the system administrator.
          </Typography.Paragraph>
        </>
      )}
    />
  );
};

export default AccessDeniedPage;
