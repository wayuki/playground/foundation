import logger from 'logger';
import React, { PureComponent } from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import { getServiceProps } from 'service-props';
import * as uuid from 'uuid';
import AccessDeniedPage from './components/AccessDeniedPage';
import GeneralErrorPage from './components/GeneralErrorPage';
import NotFoundPage from './components/NotFoundPage';

interface IErrorPageState {
  error: Error | null
}

class ErrorPage extends PureComponent<{}, IErrorPageState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      error: null,
    };
  }

  public static getDerivedStateFromError(error: Error) {
    return {
      error,
    };
  }

  public componentDidCatch() {}

  public render() {
    const { singleSpa, router: { history } } = getServiceProps();

    const { error } = this.state;
    if (error) {
      const id = uuid.v4();
      logger.error(error, { id });

      if (window.location.pathname !== '/500') {
        singleSpa.navigateToUrl(`/500?error-id=${id}`);
      }
    }

    return (
      <Router history={history}>
        <Switch>
          <Route
            path="/403"
            exact
            component={AccessDeniedPage}
          />
          <Route
            path="/500"
            exact
            component={GeneralErrorPage}
          />
          <Route
            component={NotFoundPage}
          />
        </Switch>
      </Router>
    );
  }
}

export default ErrorPage;
