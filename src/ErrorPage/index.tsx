import React from 'react';
import ReactDOM from 'react-dom';
import singleSpaReact from 'single-spa-react';
import { IServiceProps, setServiceProps } from 'service-props';
import ErrorPage from './ErrorPage';

const domElementGetter = () => document.querySelector('#content')!;

const reactLifeCycles = singleSpaReact({
  React,
  ReactDOM,
  rootComponent: ErrorPage,
  domElementGetter,
});

const bootstrap = (props: IServiceProps) => {
  setServiceProps(props);

  return reactLifeCycles.bootstrap(props);
};

const { mount, unmount } = reactLifeCycles;

export {
  bootstrap,
  mount,
  unmount,
};
