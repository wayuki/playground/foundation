/* eslint-disable import/no-extraneous-dependencies */

import { History } from 'history';
import { Action, Store } from 'redux';
import * as singleSpa from 'single-spa';

interface IKeys {}

interface IGlobalStore {
  stores: Record<string, any>
  getStore<TStore extends Store>(storeName: string): TStore
  dispatch<TAction extends Action>(action: TAction): void
}

interface ITokens {
  idToken: string | null
  accessToken: string | null
}

interface IUserInfo {
  email: string | null
}

interface IUserAccess {
  scope: string[]
}

interface IAuth {
  getTokens: (opt?: { forAuth?: boolean }) => ITokens
  getUserInfo: () => IUserInfo
  getUserAccess: () => IUserAccess
  logout: (opt?: { withRedirect?: boolean }) => void
}

interface IRouter {
  history: History
}

interface IPageManagement {
  setPageTitle: (newPageTitle: string) => void
  resetPageTitle: () => void
}

interface IServiceProps {
  name: string
  singleSpa: typeof singleSpa
  keys: IKeys
  auth: IAuth
  router: IRouter
  pageManagement: IPageManagement
  globalStore: IGlobalStore
}

let serviceProps: IServiceProps | undefined;

const getServiceProps = () => {
  if (!serviceProps) {
    throw new Error('Service Props are not initialized');
  }

  return serviceProps;
};

const getServiceProp = <N extends keyof IServiceProps>(propName: N): IServiceProps[N] => (
  getServiceProps()[propName]
);

const setServiceProps = (newServiceProps: IServiceProps) => {
  serviceProps = newServiceProps;
};

export {
  getServiceProps,
  getServiceProp,
  setServiceProps,
  IServiceProps,
};
