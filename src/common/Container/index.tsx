import React from 'react';
import PropTypes from 'prop-types';
import style from './index.module.scss';

const Container: React.FC = ({ children }) => (
  <div className={style.container}>{children}</div>
);

Container.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]),
};

Container.defaultProps = {
  children: null,
};

export default Container;
