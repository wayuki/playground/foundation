import React, { useCallback, useMemo } from 'react';
import { getServiceProps } from 'service-props';
import PropTypes from 'prop-types';

enum RouteType {
  Internal,
  External
}

const internalRoutes = {
  home: '/',
  about: '/about',
};
const externalRoutes = {
  gitlab: 'https://gitlab.com/exchange321',
};

const routes: typeof internalRoutes & typeof externalRoutes = {
  ...internalRoutes,
  ...externalRoutes,
};

const replaceSlug = <IMapping extends Record<string, string>>(route: string, mapping: IMapping) => (
  Object.entries(mapping)
    .reduce(
      (acc, [slug, value]) => (
        acc.replace(`:${slug}`, value)
      ),
      route,
    )
);

const urlForInternalRoute = <IMapping extends Record<string, string> = {}>(
  route: keyof typeof internalRoutes,
  mapping: IMapping = {} as IMapping,
) => {
  const url = replaceSlug<IMapping>(internalRoutes[route], mapping);

  return url;
};

const urlForExternalRoute = <IMapping extends Record<string, string> = {}>(
  route: keyof typeof externalRoutes,
  mapping: IMapping = {} as IMapping,
) => {
  const url = replaceSlug<IMapping>(externalRoutes[route], mapping);

  return url;
};

interface ILinkProps extends React.HTMLProps<HTMLAnchorElement> {
  to?: keyof typeof routes
  mapping?: Record<string, string>
}

const Link: React.FC<ILinkProps> = ({
  to, mapping, href, target: anchorTarget, onClick = () => {}, children, ...props
}) => {
  const { router: { history } } = getServiceProps();
  const [url, routeType] = useMemo<[string | undefined, RouteType]>(
    () => {
      if (!to) {
        return [href, RouteType.External];
      }

      if (internalRoutes[to]) {
        return [
          urlForInternalRoute(to as keyof typeof internalRoutes, mapping),
          RouteType.Internal,
        ];
      }

      if (externalRoutes[to]) {
        return [
          urlForExternalRoute(to as keyof typeof externalRoutes, mapping),
          RouteType.External,
        ];
      }

      return [href || to, RouteType.External];
    },
    [to, href],
  );

  const handleClick = useCallback<React.MouseEventHandler<HTMLAnchorElement>>(
    (e) => {
      if (routeType !== RouteType.External) {
        e.preventDefault();
      }

      onClick(e);

      if (routeType === RouteType.External) {
        return;
      }

      if (!url) {
        return;
      }

      history.push(url);
    },
    [routeType, url, onClick],
  );

  let target = anchorTarget;
  if (routeType === RouteType.External) {
    target = '_blank';
  }

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <a href={url} {...props} target={target} onClick={handleClick}>{children}</a>
  );
};

Link.propTypes = {
  to: PropTypes.string,
  mapping: PropTypes.object,
  href: PropTypes.string,
  target: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
} as React.WeakValidationMap<ILinkProps>;

export {
  RouteType,
  routes,
  urlForInternalRoute,
  urlForExternalRoute,
};

export default Link;
