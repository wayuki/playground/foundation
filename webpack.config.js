const webpack = require('webpack');
const loaderUtils = require('loader-utils');
const fs = require('fs');
const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const WebpackBar = require('webpackbar');
const ManifestPlugin = require('webpack-manifest-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const safePostCssParser = require('postcss-safe-parser');
const postcssNormalize = require('postcss-normalize');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const OrchestratorManifestWebpackPlugin = require('@wayuki/dev-utils/orchestrator-manifest-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

const appDirectory = fs.realpathSync(process.cwd());
const ensureSlash = (inputPath, needsSlash) => {
  const hasSlash = inputPath.endsWith('/');
  if (hasSlash && !needsSlash) {
    return inputPath.substr(0, inputPath.length - 1);
  }

  if (!hasSlash && needsSlash) {
    return `${inputPath}/`;
  }

  return inputPath;
};

const libraries = {
  'error-page': [
    path.resolve(appDirectory, 'src/ErrorPage/index.tsx'),
  ],
  navigation: [
    path.resolve(appDirectory, 'src/Navigation/index.tsx'),
  ],
};

module.exports = (mode) => {
  const isDevelopment = mode === 'development';
  const isProduction = mode === 'production';

  let staticAssetPrefix = process.env.STATIC_ASSET_PREFIX || '';
  staticAssetPrefix = ensureSlash(staticAssetPrefix, staticAssetPrefix);

  const publicPath = ensureSlash(process.env.PUBLIC_URL || '/', true);
  const publicUrl = publicPath.slice(0, -1);

  const shouldUseRelativeAssetPaths = publicPath.startsWith('.');

  const env = {
    NODE_ENV: process.env.NODE_ENV || 'development',
    PUBLIC_URL: publicUrl,
    REPORT_SERVICE_ENDPOINT: process.env.REPORT_SERVICE_ENDPOINT,
  };

  const manifestPluginSeed = {};
  const orchestratorManifestSeed = {};

  return Object.entries(libraries)
    .map(
      ([moduleName, entryfiles]) => ({
        mode: isProduction ? 'production' : isDevelopment && 'development',
        bail: isProduction,
        devtool: isProduction ? 'source-map' : isDevelopment && 'cheap-module-source-map',
        entry: {
          [moduleName]: [
            require.resolve('core-js/stable/index.js'),
            require.resolve('regenerator-runtime/runtime.js'),
            ...entryfiles,
          ],
        },
        output: {
          path: path.resolve(appDirectory, 'dist'),
          library: moduleName,
          libraryTarget: 'window',
          pathinfo: isDevelopment,
          filename: isProduction
            ? `${staticAssetPrefix}static/js/[name].${moduleName}.[contenthash:8].js`
            : isDevelopment && `${staticAssetPrefix}static/js/[name].${moduleName}.js`,
          futureEmitAssets: true,
          chunkFilename: isProduction
            ? `${staticAssetPrefix}static/js/[name].${moduleName}.[contenthash:8].chunk.js`
            : isDevelopment && `${staticAssetPrefix}static/js/[name].${moduleName}.chunk.js`,
          publicPath,
          devtoolModuleFilenameTemplate: isProduction
            ? (info) => path
              .relative(path.resolve(appDirectory, 'src'), info.absoluteResourcePath)
              .replace(/\\/g, '/')
            : isDevelopment && ((info) => path.resolve(info.absoluteResourcePath).replace(/\\/g, '/')),
        },
        optimization: {
          minimize: isProduction,
          minimizer: [
            new TerserPlugin({
              terserOptions: {
                parse: {
                  ecma: 8,
                },
                compress: {
                  ecma: 5,
                  warnings: false,
                  comparisons: false,
                  inline: 2,
                },
                mangle: {
                  safari10: true,
                },
                output: {
                  ecma: 5,
                  comments: false,
                  ascii_only: true,
                },
              },
              sourceMap: true,
            }),
            new OptimizeCSSAssetsPlugin({
              cssProcessorOptions: {
                parser: safePostCssParser,
                map: {
                  inline: false,
                  annotation: true,
                },
              },
              cssProcessorPluginOptions: {
                preset: ['default', { minifyFontValues: { removeQuotes: false } }],
              },
            }),
          ],
          splitChunks: {
            chunks: 'all',
            name: false,
          },
          runtimeChunk: {
            name: (entrypoint) => `runtime-${entrypoint.name}`,
          },
        },
        resolve: {
          modules: [
            'node_modules',
            path.resolve(appDirectory, 'node_modules'),
            path.resolve(appDirectory, 'src'),
          ],
          extensions: ['.js', '.jsx', '.ts', '.tsx'],
          alias: {
            antd: path.resolve(appDirectory, 'node_modules/antd/es/index.js'),
          },
        },
        module: {
          strictExportPresence: true,
          rules: [
            {
              parser: {
                requireEnsure: false,
              },
            },
            {
              test: /\.(js|jsx|ts|tsx)$/,
              enforce: 'pre',
              include: path.resolve(appDirectory, 'src'),
              loader: require.resolve('eslint-loader'),
            },
            {
              oneOf: [
                {
                  test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
                  loader: require.resolve('url-loader'),
                  options: {
                    limit: 10000,
                    name: `${staticAssetPrefix}static/media/[name].${moduleName}.[hash:8].[ext]`,
                  },
                },
                {
                  test: /\.(js|jsx|ts|tsx)$/,
                  include: path.resolve(appDirectory, 'src'),
                  loader: require.resolve('babel-loader'),
                  options: {
                    cacheDirectory: true,
                    cacheCompression: false,
                    compact: isProduction,
                  },
                },
                {
                  test: /\.(scss|sass)$/,
                  use: [
                    isDevelopment && require.resolve('style-loader'),
                    isProduction && {
                      loader: MiniCssExtractPlugin.loader,
                      options: shouldUseRelativeAssetPaths ? { publicPath: '../../' } : {},
                    },
                    {
                      loader: require.resolve('css-loader'),
                      options: {
                        importLoaders: 2,
                        sourceMap: isProduction,
                        modules: {
                          auto: true,
                          getLocalIdent: (context, localIdentName, localName, options) => {
                            let fileNameOrFolder = '[name]';
                            if (context.resourcePath.match(/index\.module\.(css|scss|sass)$/)) {
                              fileNameOrFolder = '[folder]';
                            }

                            const hash = loaderUtils.getHashDigest(
                              path.posix.relative(context.rootContext, context.resourcePath) + localName,
                              'md5',
                              'base64',
                              5,
                            );

                            const className = loaderUtils.interpolateName(
                              context,
                              fileNameOrFolder + '_' + localName + '__' + hash,
                              options,
                            );

                            return className.replace('.module_', '_').replace(/\./g, '_');
                          },
                        },
                      },
                    },
                    {
                      loader: require.resolve('postcss-loader'),
                      options: {
                        postcssOptions: {
                          ident: 'postcss',
                          plugins: [
                            require('postcss-flexbugs-fixes'),
                            require('postcss-preset-env')({
                              autoprefixer: {
                                flexbox: 'no-2009',
                              },
                              stage: 3,
                            }),
                            postcssNormalize(),
                          ],
                        },
                        sourceMap: isProduction,
                      },
                    },
                    {
                      loader: require.resolve('resolve-url-loader'),
                      options: {
                        sourceMap: isProduction,
                      },
                    },
                    {
                      loader: require.resolve('sass-loader'),
                      options: {
                        sourceMap: isProduction,
                      },
                    },
                  ].filter(Boolean),
                  sideEffects: true,
                },
                {
                  loader: require.resolve('file-loader'),
                  exclude: /\.(js|mjs|jsx|html|hbs|json)$/,
                  options: {
                    name: `${staticAssetPrefix}static/media/[name].${moduleName}.[hash:8].[ext]`,
                  },
                },
              ],
            },
          ],
        },
        plugins: [
          isDevelopment && new WebpackBar({
            name: moduleName,
          }),
          new webpack.DefinePlugin({
            'process.env': Object.entries(env)
              .reduce(
                (acc, [key, value]) => {
                  acc[key] = JSON.stringify(value);

                  return acc;
                },
                {},
              ),
          }),
          isProduction && new MiniCssExtractPlugin({
            filename: `${staticAssetPrefix}static/css/[name].${moduleName}.[contenthash:8].css`,
            chunkFilename: `${staticAssetPrefix}static/css/[name].${moduleName}.[contenthash:8].chunk.css`,
          }),
          new OrchestratorManifestWebpackPlugin({
            filename: 'playground-manifest.json',
            seed: orchestratorManifestSeed,
          }),
          new ManifestPlugin({
            fileName: 'asset-manifest.json',
            publicPath,
            seed: manifestPluginSeed,
            generate: (seed, files, entrypoints) => {
              const manifestFiles = files.reduce((manifest, file) => {
                manifest[file.name] = file.path;

                return manifest;
              }, seed);
              const entrypointFiles = entrypoints[moduleName].filter(
                (fileName) => !fileName.endsWith('.map'),
              );

              return {
                files: manifestFiles,
                entrypoints: entrypointFiles,
              };
            },
          }),
          new ForkTsCheckerWebpackPlugin({
            typescript: {
              configFile: path.resolve(appDirectory, 'tsconfig.json'),
            },
            async: isDevelopment,
          }),
        ].filter(Boolean),
        node: {
          module: 'empty',
          dgram: 'empty',
          dns: 'mock',
          fs: 'empty',
          http2: 'empty',
          net: 'empty',
          tls: 'empty',
          child_process: 'empty',
        },
        performance: false,
      }),
    );
};
