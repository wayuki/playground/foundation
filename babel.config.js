const path = require('path')

module.exports = (api) => {
  api.cache(true)

  const env = process.env.BABEL_ENV || process.env.NODE_ENV

  const isEnvDevelopment = env === 'development'
  const isEnvProduction = env === 'production'
  const isEnvTest = env === 'test'

  const absoluteRuntimePath = path.dirname(require.resolve('@babel/runtime/package.json'))

  return {
    presets: [
      [
        require('@babel/preset-env').default,
        {
          useBuiltIns: 'entry',
          corejs: 3,
          modules: false,
          exclude: ['transform-typeof-symbol']
        }
      ],
      [
        require('@babel/preset-react').default,
        {
          development: isEnvDevelopment || isEnvTest,
          useBuiltIns: true
        }
      ],
      require('@babel/preset-typescript').default
    ],
    plugins: [
      [
        require('@babel/plugin-transform-destructuring').default,
        {
          loose: false,
          selectiveLoose: [
            'useState',
            'useEffect',
            'useContext',
            'useReducer',
            'useCallback',
            'useMemo',
            'useRef',
            'useImperativeHandle',
            'useLayoutEffect',
            'useDebugValue'
          ]
        }
      ],
      [
        require('@babel/plugin-proposal-decorators').default,
        false
      ],
      [
        require('@babel/plugin-proposal-class-properties').default,
        {
          loose: true
        }
      ],
      [
        require('@babel/plugin-proposal-object-rest-spread').default,
        {
          useBuiltIns: true
        }
      ],
      [
        require('@babel/plugin-transform-runtime').default,
        {
          corejs: false,
          helpers: true,
          regenerator: true,
          useESModules: isEnvDevelopment || isEnvProduction,
          absoluteRuntime: absoluteRuntimePath
        }
      ],
      isEnvProduction && [
        require('babel-plugin-transform-react-remove-prop-types').default,
        {
          removeImport: true
        }
      ]
    ].filter(Boolean),
    overrides: [
      {
        test: /\.tsx?$/,
        plugins: [
          [
            require('@babel/plugin-proposal-decorators').default,
            {
              legacy: true
            }
          ]
        ]
      }
    ]
  }
}
