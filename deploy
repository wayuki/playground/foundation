#!/usr/bin/env python

import mimetypes
import re
import os
import sys
from time import time

import boto3
import docker
import logging
from botocore.exceptions import ClientError
from inflection import dasherize

logging.basicConfig(
    level=logging.INFO,
    stream=sys.stdout,
    format='%(asctime)s %(levelname)-8s %(message)s'
)
logging.getLogger('boto3.resources.action').setLevel(logging.WARNING)
logger = logging.getLogger()

BUILD_DIRECTORY_PATH = './dist'
CONTAINER_BUILD_DIRECTORY_PATH = f'/usr/src/app/{BUILD_DIRECTORY_PATH}'

local_cfn_dir = os.getenv('LOCAL_CFN_DIR', './cloudformation')
app_name = os.getenv('APP_NAME')
env_type = os.getenv('ENV_TYPE')
cfn_templates_bucket = os.getenv('CFN_TEMPLATES_BUCKET')


class Deployer:
    @classmethod
    def deploy(cls):
        cls.upload_cfn_templates()
        cls.run_cfn_stack(
            'master',
            'base',
            cfn_params=dict(
                appName=app_name,
                envType=env_type
            )
        )

        foundation_url = cls.get_param_store_value('/playground/shared/FOUNDATION_URL')
        static_asset_prefix = os.getenv('STATIC_ASSET_PREFIX')

        cls.build_static_files(
            os.getenv('RELEASE_IMAGE'),
            command='node build',
            environment=[
                f'PUBLIC_URL={foundation_url}',
                f'STATIC_ASSET_PREFIX={static_asset_prefix}',
            ],
            build_folder_in_host=f'{os.getenv("ROOT_DIRECTORY")}/{BUILD_DIRECTORY_PATH}',
            build_folder_in_container=CONTAINER_BUILD_DIRECTORY_PATH
        )
        if not os.listdir(path=BUILD_DIRECTORY_PATH):
            logger.error('Build directory is empty.')
            sys.exit(1)

        bucket_name = cls.get_stack_output('base', 's3BucketId')
        distribution_id = cls.get_stack_output('base', 'cloudFrontDistributionId')
        excluded_files_from_cache = ['playground-manifest.json', 'asset-manifest.json']

        cls.upload_to_s3(
            BUILD_DIRECTORY_PATH,
            bucket_name=bucket_name,
            cache_excludes=excluded_files_from_cache
        )

        cls.invalidate_cache(distribution_id, cache_excludes=excluded_files_from_cache)

    @classmethod
    def upload_cfn_templates(cls):
        logger.info('Uploading CloudFormation templates...')

        file_names = [
            f
            for (_, _, fs) in os.walk('./cloudformation')
            for f in fs
        ]

        for file_name in file_names:
            key = f'{app_name}/{env_type}/{file_name}'
            file_path = f'{local_cfn_dir}/{file_name}'

            s3 = boto3.client('s3')
            s3.upload_file(file_path, cfn_templates_bucket, key)

    @classmethod
    def run_cfn_stack(cls, template='master', stack_key=None, cfn_params=dict()):
        if stack_key is None:
            stack_key = template

        stack_name = cls.get_app_stack_name(stack_key)

        CFNStackRunner(
            stack_name,
            template,
            tags=dict(app=app_name, env=env_type),
            **cfn_params
        ).run()

    @classmethod
    def build_static_files(
        cls,
        image,
        command,
        environment,
        build_folder_in_host,
        build_folder_in_container,
        timeout=600
    ):
        logger.info('Building artefact...')

        client = docker.DockerClient(
            base_url=os.getenv('DOCKER_HOST', 'unix:///var/run/docker.sock'),
            timeout=timeout
        )

        for line in client.containers.run(
            image,
            remove=True,
            stream=True,
            command=command,
            environment=environment,
            volumes={
                build_folder_in_host: dict(bind=build_folder_in_container, mode='rw')
            }
        ):
            print(line.decode())

    @classmethod
    def upload_to_s3(cls, root_path, bucket_name, key_prefix='', cache_excludes=[]):
        logger.info('Uploading artefact to S3...')

        s3 = boto3.client('s3')
        mimetypes.add_type('application/javascript', '.js')

        filenames = []
        for path, subdirs, files in os.walk(root_path):
            path = path.replace('\\', '/')
            directory_name = re.sub(root_path + '\/?', '', path)
            if directory_name:
                directory_name = directory_name + '/'

            for file in files:
                file_path = os.path.join(path, file)
                key = key_prefix + directory_name + file
                content_type = mimetypes.guess_type(file)[0] or 'text/plain'
                cache_control = 'max-age=2592000,public'
                if file in cache_excludes:
                    cache_control = 'max-age=0,no-cache,no-store,must-revalidate'

                filename = (file_path, key, content_type, cache_control)
                filenames.append(filename)

        for filename, key, content_type, cache_control in filenames:
            s3.upload_file(
                filename,
                bucket_name,
                key,
                ExtraArgs=dict(
                    ContentType=content_type,
                    CacheControl=cache_control
                )
            )

    @classmethod
    def invalidate_cache(cls, distribution_id, cache_excludes=[]):
        logger.info('Invalidating CF cache...')

        if cache_excludes:
            cloudfront = boto3.client('cloudfront')

            items = list(map(lambda f: f'/{f}', cache_excludes))

            cloudfront.create_invalidation(
                DistributionId=distribution_id,
                InvalidationBatch=dict(
                    Paths=dict(
                        Quantity=len(items),
                        Items=items
                    ),
                    CallerReference=str(time()).replace('.', '')
                )
            )

    @classmethod
    def get_stack_output(cls, stack_name, key, app_specific=True):
        if app_specific:
            stack_name = cls.get_app_stack_name(stack_name)

        cloudformation = boto3.client('cloudformation')

        stack = cloudformation.describe_stacks(StackName=stack_name)['Stacks'][0]

        return next((output['OutputValue'] for output in stack['Outputs'] if output['OutputKey'] == key), None)

    @classmethod
    def get_app_stack_name(cls, stack_key=None):
        if not stack_key:
            return dasherize(f'{app_name}-{env_type}')

        return dasherize(f'{app_name}-{stack_key}-{env_type}')

    @classmethod
    def get_param_store_value(cls, param_name):
        ssm = boto3.client('ssm')

        try:
            result = ssm.get_parameter(Name=param_name, WithDecryption=True)['Parameter']

            return result['Value'] if result else None
        except Exception as e:
            logger.error(e)

            return None


class CFNStackRunner:
    def __init__(self, stack_name, template_name, tags={}, **params):
        self.cloudformation = boto3.client('cloudformation')
        self.stack_name = stack_name
        self.template_name = template_name
        self.tags = tags
        self.params = [dict(ParameterKey=key, ParameterValue=value) for key, value in params.items()]

    @property
    def template_root(self):
        return f'https://s3.amazonaws.com/{cfn_templates_bucket}'

    @property
    def template_project_url(self):
        return f'{self.template_root}/{app_name}/{env_type}'

    @property
    def root_cfn_template_url(self):
        return f'{self.template_project_url}/{self.template_name}.yml'

    def run(self):
        try:
            stack_status = self.cloudformation.describe_stacks(StackName=self.stack_name)['Stacks'][0]['StackStatus']
            logger.info(f'[Status] {stack_status}')

            self._update_stack()
        except ClientError as e:
            if self._is_cloudformation_no_update(e):
                logger.info('CloudFormation unchanged')
            else:
                logger.warning(e)
                self._create_stack()

    def _update_stack(self):
        logger.info('Updating stack...')
        self.cloudformation.update_stack(
            StackName=self.stack_name,
            TemplateURL=self.root_cfn_template_url,
            Parameters=self.params,
            Capabilities=['CAPABILITY_NAMED_IAM']
        )
        self._wait('stack_update_complete')

    def _create_stack(self):
        logger.info('Creating stack...')
        self.cloudformation.create_stack(
            StackName=self.stack_name,
            TemplateURL=self.root_cfn_template_url,
            Parameters=self.params,
            Tags=[dict(Key=key, Value=value) for key, value in self.tags.items()],
            Capabilities=['CAPABILITY_NAMED_IAM']
        )
        self._wait('stack_create_complete')

    def _wait(self, type):
        waiter = self.cloudformation.get_waiter(type)

        waiter.wait(StackName=self.stack_name)

    def _is_cloudformation_no_update(self, e):
        error = e.response['Error']

        return error['Code'] == 'ValidationError' and error['Message'] == 'No updates are to be performed.'


if __name__ == '__main__':
    Deployer.deploy()
